/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import type { Configuration } from '../../configuration';
import type { AxiosPromise, AxiosInstance, RawAxiosRequestConfig } from 'axios';
import globalAxios from 'axios';
// Some imports not used depending on template conditions
// @ts-ignore
import { DUMMY_BASE_URL, assertParamExists, setApiKeyToObject, setBasicAuthToObject, setBearerAuthToObject, setOAuthToObject, setSearchParams, serializeDataIfNeeded, toPathString, createRequestFunction } from '../../common';
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, type RequestArgs, BaseAPI, RequiredError, operationServerMap } from '../../base';
// @ts-ignore
import type { ApiTokenDtoPagedResponse } from '../../@coscine/model';
// @ts-ignore
import type { ApiTokenDtoResponse } from '../../@coscine/model';
// @ts-ignore
import type { ApiTokenForCreationDto } from '../../@coscine/model';
/**
 * SelfApiTokenApi - axios parameter creator
 * @export
 */
export const SelfApiTokenApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @summary Responds with the HTTP methods allowed for the endpoint.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiV2SelfApiTokensOptions: async (options: RawAxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/v2/self/api-tokens`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'OPTIONS', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            await setApiKeyToObject(localVarHeaderParameter, "Authorization", configuration)


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Creates an API token for the current authenticated user.
         * @param {ApiTokenForCreationDto} [apiTokenForCreationDto] The API token data for creation.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        createApiToken: async (apiTokenForCreationDto?: ApiTokenForCreationDto, options: RawAxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/v2/self/api-tokens`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            await setApiKeyToObject(localVarHeaderParameter, "Authorization", configuration)


    
            localVarHeaderParameter['Content-Type'] = 'application/json';

            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = serializeDataIfNeeded(apiTokenForCreationDto, localVarRequestOptions, configuration)

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Retrieves all API tokens for the current authenticated user.
         * @param {number} [pageNumber] The desired page number. Should be greater than or equal to 1. Default is 1.
         * @param {number} [pageSize] The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10.
         * @param {string} [orderBy] Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getAllApiTokens: async (pageNumber?: number, pageSize?: number, orderBy?: string, options: RawAxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/api/v2/self/api-tokens`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            await setApiKeyToObject(localVarHeaderParameter, "Authorization", configuration)

            if (pageNumber !== undefined) {
                localVarQueryParameter['PageNumber'] = pageNumber;
            }

            if (pageSize !== undefined) {
                localVarQueryParameter['PageSize'] = pageSize;
            }

            if (orderBy !== undefined) {
                localVarQueryParameter['OrderBy'] = orderBy;
            }


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Retrieves an API token for the current authenticated user.
         * @param {string} apiTokenId The ID of the token.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getApiToken: async (apiTokenId: string, options: RawAxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'apiTokenId' is not null or undefined
            assertParamExists('getApiToken', 'apiTokenId', apiTokenId)
            const localVarPath = `/api/v2/self/api-tokens/{apiTokenId}`
                .replace(`{${"apiTokenId"}}`, encodeURIComponent(String(apiTokenId)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            await setApiKeyToObject(localVarHeaderParameter, "Authorization", configuration)


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary Revokes an API token for the current authenticated user.
         * @param {string} apiTokenId The ID of the token.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        revokeToken: async (apiTokenId: string, options: RawAxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'apiTokenId' is not null or undefined
            assertParamExists('revokeToken', 'apiTokenId', apiTokenId)
            const localVarPath = `/api/v2/self/api-tokens/{apiTokenId}`
                .replace(`{${"apiTokenId"}}`, encodeURIComponent(String(apiTokenId)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, DUMMY_BASE_URL);
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }

            const localVarRequestOptions = { method: 'DELETE', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Bearer required
            await setApiKeyToObject(localVarHeaderParameter, "Authorization", configuration)


    
            setSearchParams(localVarUrlObj, localVarQueryParameter);
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: toPathString(localVarUrlObj),
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * SelfApiTokenApi - functional programming interface
 * @export
 */
export const SelfApiTokenApiFp = function(configuration?: Configuration) {
    const localVarAxiosParamCreator = SelfApiTokenApiAxiosParamCreator(configuration)
    return {
        /**
         * 
         * @summary Responds with the HTTP methods allowed for the endpoint.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async apiV2SelfApiTokensOptions(options?: RawAxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.apiV2SelfApiTokensOptions(options);
            const localVarOperationServerIndex = configuration?.serverIndex ?? 0;
            const localVarOperationServerBasePath = operationServerMap['SelfApiTokenApi.apiV2SelfApiTokensOptions']?.[localVarOperationServerIndex]?.url;
            return (axios, basePath) => createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration)(axios, localVarOperationServerBasePath || basePath);
        },
        /**
         * 
         * @summary Creates an API token for the current authenticated user.
         * @param {ApiTokenForCreationDto} [apiTokenForCreationDto] The API token data for creation.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async createApiToken(apiTokenForCreationDto?: ApiTokenForCreationDto, options?: RawAxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<ApiTokenDtoResponse>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.createApiToken(apiTokenForCreationDto, options);
            const localVarOperationServerIndex = configuration?.serverIndex ?? 0;
            const localVarOperationServerBasePath = operationServerMap['SelfApiTokenApi.createApiToken']?.[localVarOperationServerIndex]?.url;
            return (axios, basePath) => createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration)(axios, localVarOperationServerBasePath || basePath);
        },
        /**
         * 
         * @summary Retrieves all API tokens for the current authenticated user.
         * @param {number} [pageNumber] The desired page number. Should be greater than or equal to 1. Default is 1.
         * @param {number} [pageSize] The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10.
         * @param {string} [orderBy] Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getAllApiTokens(pageNumber?: number, pageSize?: number, orderBy?: string, options?: RawAxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<ApiTokenDtoPagedResponse>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getAllApiTokens(pageNumber, pageSize, orderBy, options);
            const localVarOperationServerIndex = configuration?.serverIndex ?? 0;
            const localVarOperationServerBasePath = operationServerMap['SelfApiTokenApi.getAllApiTokens']?.[localVarOperationServerIndex]?.url;
            return (axios, basePath) => createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration)(axios, localVarOperationServerBasePath || basePath);
        },
        /**
         * 
         * @summary Retrieves an API token for the current authenticated user.
         * @param {string} apiTokenId The ID of the token.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getApiToken(apiTokenId: string, options?: RawAxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<ApiTokenDtoResponse>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.getApiToken(apiTokenId, options);
            const localVarOperationServerIndex = configuration?.serverIndex ?? 0;
            const localVarOperationServerBasePath = operationServerMap['SelfApiTokenApi.getApiToken']?.[localVarOperationServerIndex]?.url;
            return (axios, basePath) => createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration)(axios, localVarOperationServerBasePath || basePath);
        },
        /**
         * 
         * @summary Revokes an API token for the current authenticated user.
         * @param {string} apiTokenId The ID of the token.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async revokeToken(apiTokenId: string, options?: RawAxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<void>> {
            const localVarAxiosArgs = await localVarAxiosParamCreator.revokeToken(apiTokenId, options);
            const localVarOperationServerIndex = configuration?.serverIndex ?? 0;
            const localVarOperationServerBasePath = operationServerMap['SelfApiTokenApi.revokeToken']?.[localVarOperationServerIndex]?.url;
            return (axios, basePath) => createRequestFunction(localVarAxiosArgs, globalAxios, BASE_PATH, configuration)(axios, localVarOperationServerBasePath || basePath);
        },
    }
};

/**
 * SelfApiTokenApi - factory interface
 * @export
 */
export const SelfApiTokenApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    const localVarFp = SelfApiTokenApiFp(configuration)
    return {
        /**
         * 
         * @summary Responds with the HTTP methods allowed for the endpoint.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        apiV2SelfApiTokensOptions(options?: RawAxiosRequestConfig): AxiosPromise<void> {
            return localVarFp.apiV2SelfApiTokensOptions(options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Creates an API token for the current authenticated user.
         * @param {SelfApiTokenApiCreateApiTokenRequest} requestParameters Request parameters.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        createApiToken(requestParameters: SelfApiTokenApiCreateApiTokenRequest = {}, options?: RawAxiosRequestConfig): AxiosPromise<ApiTokenDtoResponse> {
            return localVarFp.createApiToken(requestParameters.apiTokenForCreationDto, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Retrieves all API tokens for the current authenticated user.
         * @param {SelfApiTokenApiGetAllApiTokensRequest} requestParameters Request parameters.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getAllApiTokens(requestParameters: SelfApiTokenApiGetAllApiTokensRequest = {}, options?: RawAxiosRequestConfig): AxiosPromise<ApiTokenDtoPagedResponse> {
            return localVarFp.getAllApiTokens(requestParameters.pageNumber, requestParameters.pageSize, requestParameters.orderBy, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Retrieves an API token for the current authenticated user.
         * @param {SelfApiTokenApiGetApiTokenRequest} requestParameters Request parameters.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getApiToken(requestParameters: SelfApiTokenApiGetApiTokenRequest, options?: RawAxiosRequestConfig): AxiosPromise<ApiTokenDtoResponse> {
            return localVarFp.getApiToken(requestParameters.apiTokenId, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary Revokes an API token for the current authenticated user.
         * @param {SelfApiTokenApiRevokeTokenRequest} requestParameters Request parameters.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        revokeToken(requestParameters: SelfApiTokenApiRevokeTokenRequest, options?: RawAxiosRequestConfig): AxiosPromise<void> {
            return localVarFp.revokeToken(requestParameters.apiTokenId, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * Request parameters for createApiToken operation in SelfApiTokenApi.
 * @export
 * @interface SelfApiTokenApiCreateApiTokenRequest
 */
export interface SelfApiTokenApiCreateApiTokenRequest {
    /**
     * The API token data for creation.
     * @type {ApiTokenForCreationDto}
     * @memberof SelfApiTokenApiCreateApiToken
     */
    readonly apiTokenForCreationDto?: ApiTokenForCreationDto
}

/**
 * Request parameters for getAllApiTokens operation in SelfApiTokenApi.
 * @export
 * @interface SelfApiTokenApiGetAllApiTokensRequest
 */
export interface SelfApiTokenApiGetAllApiTokensRequest {
    /**
     * The desired page number. Should be greater than or equal to 1. Default is 1.
     * @type {number}
     * @memberof SelfApiTokenApiGetAllApiTokens
     */
    readonly pageNumber?: number

    /**
     * The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10.
     * @type {number}
     * @memberof SelfApiTokenApiGetAllApiTokens
     */
    readonly pageSize?: number

    /**
     * Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc.
     * @type {string}
     * @memberof SelfApiTokenApiGetAllApiTokens
     */
    readonly orderBy?: string
}

/**
 * Request parameters for getApiToken operation in SelfApiTokenApi.
 * @export
 * @interface SelfApiTokenApiGetApiTokenRequest
 */
export interface SelfApiTokenApiGetApiTokenRequest {
    /**
     * The ID of the token.
     * @type {string}
     * @memberof SelfApiTokenApiGetApiToken
     */
    readonly apiTokenId: string
}

/**
 * Request parameters for revokeToken operation in SelfApiTokenApi.
 * @export
 * @interface SelfApiTokenApiRevokeTokenRequest
 */
export interface SelfApiTokenApiRevokeTokenRequest {
    /**
     * The ID of the token.
     * @type {string}
     * @memberof SelfApiTokenApiRevokeToken
     */
    readonly apiTokenId: string
}

/**
 * SelfApiTokenApi - object-oriented interface
 * @export
 * @class SelfApiTokenApi
 * @extends {BaseAPI}
 */
export class SelfApiTokenApi extends BaseAPI {
    /**
     * 
     * @summary Responds with the HTTP methods allowed for the endpoint.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SelfApiTokenApi
     */
    public apiV2SelfApiTokensOptions(options?: RawAxiosRequestConfig) {
        return SelfApiTokenApiFp(this.configuration).apiV2SelfApiTokensOptions(options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Creates an API token for the current authenticated user.
     * @param {SelfApiTokenApiCreateApiTokenRequest} requestParameters Request parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SelfApiTokenApi
     */
    public createApiToken(requestParameters: SelfApiTokenApiCreateApiTokenRequest = {}, options?: RawAxiosRequestConfig) {
        return SelfApiTokenApiFp(this.configuration).createApiToken(requestParameters.apiTokenForCreationDto, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Retrieves all API tokens for the current authenticated user.
     * @param {SelfApiTokenApiGetAllApiTokensRequest} requestParameters Request parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SelfApiTokenApi
     */
    public getAllApiTokens(requestParameters: SelfApiTokenApiGetAllApiTokensRequest = {}, options?: RawAxiosRequestConfig) {
        return SelfApiTokenApiFp(this.configuration).getAllApiTokens(requestParameters.pageNumber, requestParameters.pageSize, requestParameters.orderBy, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Retrieves an API token for the current authenticated user.
     * @param {SelfApiTokenApiGetApiTokenRequest} requestParameters Request parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SelfApiTokenApi
     */
    public getApiToken(requestParameters: SelfApiTokenApiGetApiTokenRequest, options?: RawAxiosRequestConfig) {
        return SelfApiTokenApiFp(this.configuration).getApiToken(requestParameters.apiTokenId, options).then((request) => request(this.axios, this.basePath));
    }

    /**
     * 
     * @summary Revokes an API token for the current authenticated user.
     * @param {SelfApiTokenApiRevokeTokenRequest} requestParameters Request parameters.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SelfApiTokenApi
     */
    public revokeToken(requestParameters: SelfApiTokenApiRevokeTokenRequest, options?: RawAxiosRequestConfig) {
        return SelfApiTokenApiFp(this.configuration).revokeToken(requestParameters.apiTokenId, options).then((request) => request(this.axios, this.basePath));
    }
}

