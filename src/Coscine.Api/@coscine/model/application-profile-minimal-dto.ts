/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * Represents a minimalistic application profile data transfer object.
 * @export
 * @interface ApplicationProfileMinimalDto
 */
export interface ApplicationProfileMinimalDto {
    /**
     * The URI associated with the application profile.
     * @type {string}
     * @memberof ApplicationProfileMinimalDto
     */
    'uri': string;
}

