/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { GitlabResourceTypeOptionsForUpdateDto } from './gitlab-resource-type-options-for-update-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { RdsResourceTypeOptionsForManipulationDto } from './rds-resource-type-options-for-manipulation-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { RdsS3ResourceTypeOptionsForManipulationDto } from './rds-s3-resource-type-options-for-manipulation-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { RdsS3WormResourceTypeOptionsForManipulationDto } from './rds-s3-worm-resource-type-options-for-manipulation-dto';

/**
 * Represents the data transfer object (DTO) used for updating options related to any resource type.
 * @export
 * @interface ResourceTypeOptionsForUpdateDto
 */
export interface ResourceTypeOptionsForUpdateDto {
    /**
     * Represents the data transfer object (DTO) for manipulating linked data resource type options.
     * @type {object}
     * @memberof ResourceTypeOptionsForUpdateDto
     */
    'linkedResourceTypeOptions'?: object;
    /**
     * 
     * @type {GitlabResourceTypeOptionsForUpdateDto}
     * @memberof ResourceTypeOptionsForUpdateDto
     */
    'gitlabResourceTypeOptions'?: GitlabResourceTypeOptionsForUpdateDto;
    /**
     * 
     * @type {RdsResourceTypeOptionsForManipulationDto}
     * @memberof ResourceTypeOptionsForUpdateDto
     */
    'rdsResourceTypeOptions'?: RdsResourceTypeOptionsForManipulationDto;
    /**
     * 
     * @type {RdsS3ResourceTypeOptionsForManipulationDto}
     * @memberof ResourceTypeOptionsForUpdateDto
     */
    'rdsS3ResourceTypeOptions'?: RdsS3ResourceTypeOptionsForManipulationDto;
    /**
     * 
     * @type {RdsS3WormResourceTypeOptionsForManipulationDto}
     * @memberof ResourceTypeOptionsForUpdateDto
     */
    'rdsS3WormResourceTypeOptions'?: RdsS3WormResourceTypeOptionsForManipulationDto;
}

