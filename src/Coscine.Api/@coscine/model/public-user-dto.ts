/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { TitleDto } from './title-dto';

/**
 * Represents a public user data transfer object (DTO).
 * @export
 * @interface PublicUserDto
 */
export interface PublicUserDto {
    /**
     * Unique identifier of the user.
     * @type {string}
     * @memberof PublicUserDto
     */
    'id': string;
    /**
     * Display name of the user.
     * @type {string}
     * @memberof PublicUserDto
     */
    'displayName': string;
    /**
     * Given name of the user.
     * @type {string}
     * @memberof PublicUserDto
     */
    'givenName': string;
    /**
     * Family name of the user.
     * @type {string}
     * @memberof PublicUserDto
     */
    'familyName': string;
    /**
     * Email address of the user.
     * @type {string}
     * @memberof PublicUserDto
     */
    'email': string;
    /**
     * 
     * @type {TitleDto}
     * @memberof PublicUserDto
     */
    'title'?: TitleDto;
}

