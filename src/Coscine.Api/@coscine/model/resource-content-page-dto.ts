/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { ResourceContentPageEntriesViewDto } from './resource-content-page-entries-view-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { ResourceContentPageMetadataViewDto } from './resource-content-page-metadata-view-dto';

/**
 * Represents the content page details for a resource.
 * @export
 * @interface ResourceContentPageDto
 */
export interface ResourceContentPageDto {
    /**
     * Indicates whether the resource is read-only.
     * @type {boolean}
     * @memberof ResourceContentPageDto
     */
    'readOnly'?: boolean;
    /**
     * 
     * @type {ResourceContentPageMetadataViewDto}
     * @memberof ResourceContentPageDto
     */
    'metadataView'?: ResourceContentPageMetadataViewDto;
    /**
     * 
     * @type {ResourceContentPageEntriesViewDto}
     * @memberof ResourceContentPageDto
     */
    'entriesView'?: ResourceContentPageEntriesViewDto;
}

