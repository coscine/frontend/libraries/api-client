/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { ApplicationProfileMinimalDto } from './application-profile-minimal-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { DisciplineDto } from './discipline-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { FixedValueForResourceManipulationDto } from './fixed-value-for-resource-manipulation-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { LicenseDto } from './license-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { ProjectMinimalDto } from './project-minimal-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { ProjectResourceMinimalDto } from './project-resource-minimal-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { ResourceQuotaDto } from './resource-quota-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { ResourceTypeDto } from './resource-type-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { UserMinimalDto } from './user-minimal-dto';
// May contain unused imports in some cases
// @ts-ignore
import type { VisibilityDto } from './visibility-dto';

/**
 * Represents a Data Transfer Object (DTO) for administrative purposes with additional resource details.
 * @export
 * @interface ResourceAdminDto
 */
export interface ResourceAdminDto {
    /**
     * Unique identifier for the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'id'?: string;
    /**
     * Persistent identifier for the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'pid': string;
    /**
     * 
     * @type {ResourceTypeDto}
     * @memberof ResourceAdminDto
     */
    'type': ResourceTypeDto;
    /**
     * Name of the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'name': string;
    /**
     * Display name of the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'displayName': string;
    /**
     * Description of the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'description': string;
    /**
     * Keywords associated with the resource.
     * @type {Array<string>}
     * @memberof ResourceAdminDto
     */
    'keywords'?: Array<string> | null;
    /**
     * 
     * @type {LicenseDto}
     * @memberof ResourceAdminDto
     */
    'license'?: LicenseDto;
    /**
     * Usage rights associated with the resource.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'usageRights'?: string | null;
    /**
     * Indicates whether a local copy of the metadata is available for the resource.
     * @type {boolean}
     * @memberof ResourceAdminDto
     */
    'metadataLocalCopy'?: boolean;
    /**
     * Indicates whether metadata extraction is enabled for the resource.
     * @type {boolean}
     * @memberof ResourceAdminDto
     */
    'metadataExtraction'?: boolean;
    /**
     * 
     * @type {ApplicationProfileMinimalDto}
     * @memberof ResourceAdminDto
     */
    'applicationProfile': ApplicationProfileMinimalDto;
    /**
     * Fixed values associated with resource manipulation.  This dictionary may contain multiple levels and is structured as follows:  Dictionary (Key: string) -> Dictionary (Key: string) -> List of FixedValueForResourceManipulationDto.
     * @type {{ [key: string]: { [key: string]: Array<FixedValueForResourceManipulationDto>; }; }}
     * @memberof ResourceAdminDto
     */
    'fixedValues': { [key: string]: { [key: string]: Array<FixedValueForResourceManipulationDto>; }; };
    /**
     * Disciplines associated with the resource.
     * @type {Array<DisciplineDto>}
     * @memberof ResourceAdminDto
     */
    'disciplines': Array<DisciplineDto>;
    /**
     * 
     * @type {VisibilityDto}
     * @memberof ResourceAdminDto
     */
    'visibility': VisibilityDto;
    /**
     * Date when the resource was created.
     * @type {string}
     * @memberof ResourceAdminDto
     */
    'dateCreated'?: string | null;
    /**
     * 
     * @type {UserMinimalDto}
     * @memberof ResourceAdminDto
     */
    'creator'?: UserMinimalDto;
    /**
     * Indicates whether the resource is archived.
     * @type {boolean}
     * @memberof ResourceAdminDto
     */
    'archived'?: boolean;
    /**
     * Indicates whether the resource is in maintenance mode.
     * @type {boolean}
     * @memberof ResourceAdminDto
     */
    'maintenanceMode'?: boolean;
    /**
     * The projects associated with the resource.
     * @type {Array<ProjectMinimalDto>}
     * @memberof ResourceAdminDto
     */
    'projects'?: Array<ProjectMinimalDto> | null;
    /**
     * Indicates whether the resource is deleted.
     * @type {boolean}
     * @memberof ResourceAdminDto
     */
    'deleted': boolean;
    /**
     * Collection of minimal project resource details associated with this resource.
     * @type {Array<ProjectResourceMinimalDto>}
     * @memberof ResourceAdminDto
     */
    'projectResources': Array<ProjectResourceMinimalDto>;
    /**
     * 
     * @type {ResourceQuotaDto}
     * @memberof ResourceAdminDto
     */
    'resourceQuota'?: ResourceQuotaDto;
}

