/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * Data transfer object (DTO) representing a discipline for project manipulation.
 * @export
 * @interface DisciplineForProjectManipulationDto
 */
export interface DisciplineForProjectManipulationDto {
    /**
     * Gets or initializes the identifier of the discipline.
     * @type {string}
     * @memberof DisciplineForProjectManipulationDto
     */
    'id': string;
}

