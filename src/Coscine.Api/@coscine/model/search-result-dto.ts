/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { SearchCategoryType } from './search-category-type';

/**
 * Represents a Data Transfer Object (DTO) for search results.
 * @export
 * @interface SearchResultDto
 */
export interface SearchResultDto {
    /**
     * The URI associated with the search result.
     * @type {string}
     * @memberof SearchResultDto
     */
    'uri': string;
    /**
     * 
     * @type {SearchCategoryType}
     * @memberof SearchResultDto
     */
    'type': SearchCategoryType;
    /**
     * The dynamic source data for the search result.
     * @type {any}
     * @memberof SearchResultDto
     */
    'source': any;
}



