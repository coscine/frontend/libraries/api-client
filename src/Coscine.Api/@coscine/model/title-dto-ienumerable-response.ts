/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { TitleDto } from './title-dto';

/**
 * 
 * @export
 * @interface TitleDtoIEnumerableResponse
 */
export interface TitleDtoIEnumerableResponse {
    /**
     * 
     * @type {Array<TitleDto>}
     * @memberof TitleDtoIEnumerableResponse
     */
    'data'?: Array<TitleDto> | null;
    /**
     * 
     * @type {boolean}
     * @memberof TitleDtoIEnumerableResponse
     */
    'isSuccess'?: boolean;
    /**
     * 
     * @type {number}
     * @memberof TitleDtoIEnumerableResponse
     */
    'statusCode'?: number | null;
    /**
     * 
     * @type {string}
     * @memberof TitleDtoIEnumerableResponse
     */
    'traceId'?: string | null;
}

