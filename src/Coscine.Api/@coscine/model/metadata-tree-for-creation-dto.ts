/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { RdfDefinitionForManipulationDto } from './rdf-definition-for-manipulation-dto';

/**
 * Data transfer object (DTO) representing the creation of a metadata tree.  Extends the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.MetadataTreeForManipulationDto.
 * @export
 * @interface MetadataTreeForCreationDto
 */
export interface MetadataTreeForCreationDto {
    /**
     * Gets or initializes the path of the metadata tree.
     * @type {string}
     * @memberof MetadataTreeForCreationDto
     */
    'path': string;
    /**
     * 
     * @type {RdfDefinitionForManipulationDto}
     * @memberof MetadataTreeForCreationDto
     */
    'definition': RdfDefinitionForManipulationDto;
}

