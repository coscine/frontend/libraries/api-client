/* tslint:disable */
/* eslint-disable */
/**
 * Coscine Web API
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import type { ApplicationProfileDto } from './application-profile-dto';

/**
 * 
 * @export
 * @interface ApplicationProfileDtoResponse
 */
export interface ApplicationProfileDtoResponse {
    /**
     * 
     * @type {ApplicationProfileDto}
     * @memberof ApplicationProfileDtoResponse
     */
    'data'?: ApplicationProfileDto;
    /**
     * 
     * @type {boolean}
     * @memberof ApplicationProfileDtoResponse
     */
    'isSuccess'?: boolean;
    /**
     * 
     * @type {number}
     * @memberof ApplicationProfileDtoResponse
     */
    'statusCode'?: number | null;
    /**
     * 
     * @type {string}
     * @memberof ApplicationProfileDtoResponse
     */
    'traceId'?: string | null;
}

