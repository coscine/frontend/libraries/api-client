declare const coscine: { 
    readonly authorization?: { readonly bearer?: string },
    readonly clientcorrelation?: { readonly id?: string },
    readonly loading?: { counter?: number },
};