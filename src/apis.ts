import { AxiosInstance } from 'axios';

import { Configuration } from './Coscine.Api/configuration';

// Coscine.Api APIs
import {
  AdminApiFactory,
  ApplicationProfileApiFactory,
  BlobApiFactory,
  DisciplineApiFactory,
  HandleApiFactory,
  LanguageApiFactory,
  LicenseApiFactory,
  MaintenanceApiFactory,
  OrganizationApiFactory,
  PidApiFactory,
  ProjectApiFactory,
  ProjectInvitationApiFactory,
  ProjectMemberApiFactory,
  ProjectPublicationRequestApiFactory,
  ProjectQuotaApiFactory,
  ProjectResourceApiFactory,
  ProjectResourceQuotaApiFactory,
  ProjectResourceTypeApiFactory,
  ResourceApiFactory,
  ResourceTypeApiFactory,
  RoleApiFactory,
  SearchApiFactory,
  SelfApiFactory,
  SelfApiTokenApiFactory,
  SelfSessionApiFactory,
  SystemStatusApiFactory,
  TitleApiFactory,
  TosApiFactory,
  TreeApiFactory,
  UserApiFactory,
  VisibilityApiFactory,
  VocabularyApiFactory
} from './Coscine.Api/api';

// Coscine STS APIs
import { 
  AccountApiFactory,
  HomeApiFactory,
  MergeApiFactory,
  ORCiDApiFactory,
  ShibbolethApiFactory
} from './Coscine.Api.STS/api';

let accessToken = '';
if (typeof window !== 'undefined') {
  // LocalStorage > Global Variables
  const localStorageToken = localStorage.getItem('coscine.authorization.bearer');
  if (localStorageToken) {
    accessToken = 'Bearer ' + localStorageToken;
  }
}

const getHostName = () => {
  const localStorageHostName = typeof window !== 'undefined' ? localStorage.getItem('coscine.api.hostname') : null;
  if (localStorageHostName) {
    return localStorageHostName;
  }
  let hostName = typeof window !== 'undefined' ? window.location.hostname : 'coscine.rwth-aachen.de';
  if (hostName.indexOf(':') !== -1) {
    if (hostName.indexOf('https://') !== -1) {
      hostName = hostName.replace('https://', '');
    }
    hostName = hostName.substr(0, hostName.indexOf(':'));
  }
  return hostName;
};

function implementations(axios?: AxiosInstance) {
  return {
    // Upon adding or removing any API endpoints, the list of entries below may have to be manually updated:
    AccountApi: AccountApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine/api/Coscine.Api.STS', axios),
    AdminApi: AdminApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ApplicationProfileApi: ApplicationProfileApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    BlobApi: BlobApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    DisciplineApi: DisciplineApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    HandleApi: HandleApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    HomeApi: HomeApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine/api/Coscine.Api.STS', axios),
    LanguageApi: LanguageApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    LicenseApi: LicenseApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    MaintenanceApi: MaintenanceApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    MergeApi: MergeApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine/api/Coscine.Api.STS', axios),
    ORCiDApi: ORCiDApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine/api/Coscine.Api.STS', axios),
    OrganizationApi: OrganizationApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    PidApi: PidApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectApi: ProjectApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectInvitationApi: ProjectInvitationApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectMemberApi: ProjectMemberApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectPublicationRequestApi: ProjectPublicationRequestApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectQuotaApi: ProjectQuotaApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectResourceApi: ProjectResourceApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectResourceQuotaApi: ProjectResourceQuotaApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ProjectResourceTypeApi: ProjectResourceTypeApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ResourceApi: ResourceApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ResourceTypeApi: ResourceTypeApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    RoleApi: RoleApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    SearchApi: SearchApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    SelfApi: SelfApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    SelfApiTokenApi: SelfApiTokenApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    SelfSessionApi: SelfSessionApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    ShibbolethApi: ShibbolethApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine/api/Coscine.Api.STS', axios),
    SystemStatusApi: SystemStatusApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    TitleApi: TitleApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    TosApi: TosApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    TreeApi: TreeApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    UserApi: UserApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    VisibilityApi: VisibilityApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios),
    VocabularyApi: VocabularyApiFactory(new Configuration({ 'accessToken': accessToken }), 'https://' + getHostName() + '/coscine', axios)
  };
};

export default implementations;
