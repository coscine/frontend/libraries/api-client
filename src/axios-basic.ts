import axios, { type InternalAxiosRequestConfig } from "axios";

const instance = axios.create({
  withCredentials: true
});

const clientCorrelationIdKey = 'X-Coscine-Logging-CorrelationId';

function setHeader(request: InternalAxiosRequestConfig) {
  if (typeof window !== 'undefined') {
    const localStorageToken = localStorage.getItem("coscine.authorization.bearer");
    if (localStorageToken) {
      request.headers.setAuthorization('Bearer ' + localStorageToken);
    }
    const localStorageClientCorrelation = localStorage.getItem("coscine.clientcorrelation.id");
    if (localStorageClientCorrelation) {
      request.headers.set(clientCorrelationIdKey, localStorageClientCorrelation);
    }
  }
};

instance.interceptors.request.use((request: InternalAxiosRequestConfig) => {
  setHeader(request);
  return request;
}, (error: unknown) => {
  return Promise.reject(error);
}, { synchronous: true });

export default instance;
