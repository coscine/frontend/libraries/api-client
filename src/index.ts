import implementations from './apis';
import instance from './axios-basic';

const apis = implementations(instance);

// Upon adding or removing any API endpoints, the list of entries below may have to be manually updated:
export const AccountApi = apis.AccountApi;
export const AdminApi = apis.AdminApi;
export const ApplicationProfileApi = apis.ApplicationProfileApi;
export const BlobApi = apis.BlobApi;
export const DisciplineApi = apis.DisciplineApi;
export const HandleApi = apis.HandleApi;
export const HomeApi = apis.HomeApi;
export const LanguageApi = apis.LanguageApi;
export const LicenseApi = apis.LicenseApi;
export const MaintenanceApi = apis.MaintenanceApi;
export const MergeApi = apis.MergeApi;
export const ORCiDApi = apis.ORCiDApi;
export const OrganizationApi = apis.OrganizationApi;
export const PidApi = apis.PidApi;
export const ProjectApi = apis.ProjectApi;
export const ProjectInvitationApi = apis.ProjectInvitationApi;
export const ProjectMemberApi = apis.ProjectMemberApi;
export const ProjectPublicationRequestApi = apis.ProjectPublicationRequestApi;
export const ProjectQuotaApi = apis.ProjectQuotaApi;
export const ProjectResourceApi = apis.ProjectResourceApi;
export const ProjectResourceQuotaApi = apis.ProjectResourceQuotaApi;
export const ProjectResourceTypeApi = apis.ProjectResourceTypeApi;
export const ResourceApi = apis.ResourceApi;
export const ResourceTypeApi = apis.ResourceTypeApi;
export const RoleApi = apis.RoleApi;
export const SearchApi = apis.SearchApi;
export const SelfApi = apis.SelfApi;
export const SelfApiTokenApi = apis.SelfApiTokenApi;
export const ShibbolethApi = apis.ShibbolethApi;
export const SystemStatusApi = apis.SystemStatusApi;
export const TitleApi = apis.TitleApi;
export const TosApi = apis.TosApi;
export const TreeApi = apis.TreeApi;
export const UserApi = apis.UserApi;
export const VisibilityApi = apis.VisibilityApi;
export const VocabularyApi = apis.VocabularyApi;

export default apis;
