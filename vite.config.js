import path from 'node:path';

import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    minify: true,
    lib: {
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: '@coscine/api-client',
      fileName: 'index',
      formats: ['es'],
    },
  },
  plugins: [
    dts({
      entryRoot: "src",
      outDir: "dist/types"
    }),
  ],  
  test: {
    globals: true,
    environment: 'node',
    reporters: 'dot',
  },
});
