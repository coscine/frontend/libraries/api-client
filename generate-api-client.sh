#!/bin/bash

# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Generate TypeScript API Client using OpenAPI Generator:
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# This script generates a TypeScript API client based on the OpenAPI specification.
#
# The actions performed are:
# - Generating the API client code using podman (make sure it's available).
# - Deleting existing API client source files.
# - Copying the newly generated API client source to the project directory.
# - Cleaning up temporary files.
#
# Defined variables:
OUTPUT_DIR="temp"                                     # The temporary directory for generated files
PACKAGE_NAME="Coscine.Api"                            # The package name for the API client
API_SPEC_URL="https://coscine-hristov.web.vulcanus.otc.coscine.dev/coscine/api/swagger/v2/swagger.json" # URL to the OpenAPI spec file
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------

# ANSI color codes for styling
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

# Run the OpenAPI generator
echo -e "${CYAN}Running the OpenAPI generator...${NC}"
podman run --rm \
  -v ${PWD}:/local openapitools/openapi-generator-cli generate \
  -i "$API_SPEC_URL" \
  -g typescript-axios \
  -o /local/$OUTPUT_DIR \
  --additional-properties=useSingleRequestParameter=true,apiPackage=@coscine/api,modelPackage=@coscine/model,withSeparateModelsAndApi=true,enumPropertyNaming=original \
  --skip-validate-spec

echo -e "${GREEN}API client generation complete.${NC}"

# Delete the current API client source
echo -e "${YELLOW}Deleting current API client source...${NC}"
rm -rf "src/$PACKAGE_NAME"

# Copy the generated API client source to the src directory
echo -e "${CYAN}Copying generated API client source to src directory...${NC}"
cp -r "$OUTPUT_DIR/" "src/$PACKAGE_NAME"

# Remove the temp directory
echo -e "${YELLOW}Cleaning up...${NC}"
rm -rf "$OUTPUT_DIR"

echo -e "${GREEN}Finished.${NC}"